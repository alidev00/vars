import { redirect, type Handle, type HandleFetch } from '@sveltejs/kit';
import type { ISessionInfo } from '$lib/models/interfaces/session';
import config from './config';

const parseCookieFromString = (cookiesString: string) => {
	const cookieObj: any = {};

    // Rastreo de pares clave/valor dentro del string de cookies de la página.
	if (cookiesString) {
		cookiesString
        .split(";")
		.map((str: string) => str.replaceAll(/\s/g, '').split("="))
		.forEach(([ name, content ]: string[]) => cookieObj[name] = content);
	}

    return cookieObj;
}

export const handle: Handle = async ({ event, resolve }) => {
    let info: any = event.cookies.get('_A');
    if (info) {
        try {
            const req = await event.fetch(`${config.API_PATH}/users/me`, {
                method: 'GET',
                headers: { 'Authorization': info ?? '' }
            });
            const { data: { user }, msg, errors } = await req.json();

            info = user;
        } catch (error) {
            console.log({ in: "hooks", error });
        }

        console.log({ info })
    }
    const session: ISessionInfo | null = info ?? null;

    // Restricción de rutas.
    if (session == null && !config.routesWhenNologged.some(route => event.url.pathname.startsWith('/' + route)))
        redirect(303, '/auth');
	if (session && config.routesWhenNologged.some(route => event.url.pathname.startsWith('/' + route)))
		redirect(303, '/');
    if (session && session.role == 'patient' && session.context == '' && !event.url.pathname.startsWith('/addiction-questionnaire'))
        redirect(303, '/addiction-questionnaire');
    if (session && session.role == 'patient' && session.context != '' && event.url.pathname.startsWith('/addiction-questionnaire'))
        redirect(303, '/');

    // Preparar variables para el frontend.
	event.locals = {
        error: (err: any) => {
            if (err == null)
                return null;

            return { data: null, msg: err.map((e: any) => e.description) };
        },
        session,
        title_base: config.TITLE_BASE,
        title: config.TITLE,
        api_path: config.API_PATH,
        EXP: config.EXP,
        question_string: config.question_string
    };

    return await resolve(event);
}

export const handleFetch: HandleFetch = async ({ event, request, fetch }) => {
    const cookies: string = event.request.headers.get('cookie') ?? "";
    const parsedCookies: any = parseCookieFromString(cookies);
    let req;

    // Inserción del bearer token.
    if (parsedCookies.hasOwnProperty('_A'))
        request.headers.set('Authorization', `${parsedCookies._A}`);
    
    // Ejecución del fetch solicitado.
	req = await fetch(request);

    return req;
};