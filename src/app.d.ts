import type { ISessionInfo } from '$lib/models/interfaces/session.ts';

interface EXP {
	[key: string]: any;
}

interface ILocals {
	error: any;
	session: ISessionInfo | null;
	title: string;
	title_base: string;
	api_path: string;
	question_string: any;
	EXP: EXP | null;
}

declare global {
	namespace App {
		// interface Error {}
		interface Locals extends ILocals {}
		interface LayoutData extends ILocals {}
		// interface PageState {}
		// interface Platform {}
		interface LayoutData extends ILocals {}
	}
}

export {};
