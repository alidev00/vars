export default {
    TITLE_BASE: 'VARS | ',
    TITLE: 'Virtual Addiction Recovery Support',
    API_PATH: 'http://ec2-54-158-33-254.compute-1.amazonaws.com/api/v1',
    EXP: {
        name: /^[a-zA-Z]+$/,
        lastname: /^[a-zA-Z\s]+$/,
        birthdate: /(\d{4}\-\d{2}\-\d{2})|(\d{2}\-\d{2}\-\d{4})/,
        email: /[\da-zA-Z]+(?:[_\-.][\da-zA-Z]+)*@[\da-zA-Z_\-.]+\.[\da-zA-Z]+/,
        addictionId: /^[0-9a-fA-F]{1,24}$/,
        password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,32}$/,
        phone: /\+\d+/,
        role: /^(professional|patient)$/,
        nroDoctor: /^[0-9]+$/,
        question: /^[a-zA-Z0-9.,:\s;\-_ÁÉÍÓÚáéíóú¡!¿?+*]+$/,
        title: /^[a-zA-Z0-9.,:;\-_ÁÉÍÓÚáéíóú¡!¿?+*]+$/,
        patientId: /^[0-9a-fA-F]{24}$/,
        treatment: /.+/
    },
    question_string: {
        question_1: 'Pregunta #1: ¿Cuánto tiempo has estado involucrado en esta adicción?',
		question_2: 'Pregunta #2: ¿Has experimentado dificultades en el trabajo, la escuela o en tus responsabilidades debido a tu adicción?',
		question_3: 'Pregunta #3: ¿Has intentado controlar o detener tu adicción en el pasado?',
		question_4: 'Pregunta #4: ¿Qué emociones o situaciones desencadenan el uso de la sustancia o el comportamiento adictivo?',
		question_5: 'Pregunta #5: ¿Cómo afecta tu adicción a tu salud física y mental?',
		question_6: 'Pregunta #6: ¿Has experimentado episodios de abstinencia o retirada exitosos en el pasado?',
		question_7: 'Pregunta #7: ¿Cuáles son tus principales motivos para buscar recuperación y abordar tu adicción?',
		question_8: 'Pregunta #8: ¿Cuáles son tus fortalezas personales que podrían ayudarte en el proceso de recuperación?'
    },
    routesWhenNologged: [ "auth", "auth/login", "auth/signup" ]
};