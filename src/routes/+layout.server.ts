import type { LayoutServerLoad } from './$types';

export const load: LayoutServerLoad = async ({ locals }) => {

    return {
        EXP: { ...locals.EXP },
        session: locals.session,
        title_base: locals.title_base,
        title: locals.title,
        question_string: locals.question_string
    };
}