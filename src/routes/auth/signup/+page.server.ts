import { redirect, fail } from '@sveltejs/kit';
import type { PageServerLoad, Actions } from './$types';

export const load: PageServerLoad = async ({ fetch, params, locals }) => {
    let addictions: [] = [];

    try {
		const req = await fetch(`${locals.api_path}/addictions`);
        const { data, msg, errors } = await req.json();

        if (errors != null)
            throw locals.error(errors);

        addictions = data.addictions.map((addiction: any) => ({ text: addiction.name, value: addiction.id }));
	} catch (error: any) {
        return fail(400, error);
    }

    return {
        title: "Registrarme",
        addictions
    };
}

export const actions: Actions = {
    signup: async ({ request, fetch, locals, cookies }: any) => {
        const fetchData = Object.fromEntries(await request.formData());
        const validation: any = {};
        
        Object.entries(fetchData).forEach(([key, value]: any) => {
            validation[key as keyof typeof validation] = locals.EXP[key].test(value);
        });

        if (Object.values(validation).some(val => !val))
            return fail(400, validation);

        if (fetchData.hasOwnProperty('birthdate')) {
            const realDate = new Date(fetchData.birthdate);
            realDate.setDate(realDate.getDate() + 1);
            fetchData.birthdate = realDate.getTime();            
        }
        
        try {
			const req = await fetch(`${locals.api_path}/auth/signup`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(fetchData)
			});
            const { data, msg, errors } = await req.json();
            
            if (errors != null)
                throw locals.error(errors);

            // Crear cookie de sesión.
            cookies.set('_A', data.accessToken, {
                path: '/',
                sameSite: 'none',
                httpOnly: true,
                secure: true,
                maxAge: 1000 * 60 * 60 * 24 * 5
            });
		} catch (error: any) {
			return fail(400, error);
		}

		throw redirect(302, '/');
    }
}