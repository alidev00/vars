import { redirect, fail } from '@sveltejs/kit';
import type { PageServerLoad, Actions } from './$types';

export const load: PageServerLoad = async ({ fetch, params, locals }) => {

    return {
        title: "Iniciar sesión"
    };
}

export const actions: Actions = {
    login: async ({ request, fetch, locals, cookies }: any) => {
        const fetchData = Object.fromEntries(await request.formData());
        const validation: any = {}
        
        console.log("iniciar sesion", fetchData);
        
        Object.entries(fetchData).forEach(([key, value]: any) => {
            validation[key as keyof typeof validation] = locals.EXP[key].test(value);
        });

        if (Object.values(validation).some(val => !val))
            return fail(400, validation);
        
        try {
			const req = await fetch(`${locals.api_path}/auth/signin`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(fetchData)
			});

            const { data, msg, errors } = await req.json();

            if (errors != null)
                throw locals.error(errors);

            console.log({ data, msg, errors })

            // Crear cookie de sesión.
            cookies.set('_A', data.accessToken, {
                path: '/',
                sameSite: 'none',
                httpOnly: true,
                secure: true,
                maxAge: 1000 * 60 * 60 * 24 * 5
            });
		} catch(error: any) {
			return fail(400, error);
		}

		throw redirect(302, '/');
    }
}