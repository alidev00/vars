import { redirect, fail } from '@sveltejs/kit';
import type { PageServerLoad, Actions } from './$types';

export const load: PageServerLoad = async ({ fetch, params, locals }) => {
    if (locals.session.role != 'professional' || params.patientId == '')
        redirect(302, '/');

    let patient;

    try {
        const req = await fetch(`${locals.api_path}/users/me/patients`);
        const { data, msg, errors } = await req.json();

        if (errors != null)
            throw locals.error(errors);

        patient = data.users.find((user: any) => user.id == params.patientId);

        const reqAddiction = await fetch(`${locals.api_path}/addictions/${patient.addictionId}`);
        const { data: dataAddiction } = await reqAddiction.json();

        patient.addictionName = dataAddiction.addiction.name;
    } catch (error: any) {
        return fail(400, error);
    }

    return { title: 'Caso del paciente', patient };
}

export const actions: Actions = {
    assignTreatment: async ({ request, locals, fetch }: any) => {
        const fetchData = Object.fromEntries(await request.formData());
        const validation: any = {};
        
        Object.entries(fetchData).forEach(([key, value]: any) => {
            validation[key as keyof typeof validation] = locals.EXP[key].test(value);
        });

        if (Object.values(validation).some(val => !val))
            return fail(400, validation);

        try {
            const req = await fetch(`${locals.api_path}/users/asign/treatment`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(fetchData)
            });

            const { data, msg, errors } = await req.json();

            if (errors != null)
                throw locals.error(errors);

            return { data, msg, errors };
        } catch(error: any) {
            return fail(400, error);
        }
    }
}