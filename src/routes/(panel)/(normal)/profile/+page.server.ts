import { redirect, fail } from '@sveltejs/kit';
import type { PageServerLoad, Actions } from './$types';

export const load: PageServerLoad = async ({ fetch, params, locals }) => {
    let addiction;

    if (locals.session.role == 'patient') {    
        try {
            const req = await fetch(`${locals.api_path}/users/me/addiction`);
            const { data, msg, errors } = await req.json();

            if (errors != null)
                throw locals.error(errors);

            addiction = data.addiction;
        } catch (error: any) {
            return fail(400, error);
        }
    }

    return { title: 'Perfil del usuario', addiction };
}

export const actions: Actions = {
    updateProfile: async ({ request, locals, fetch, cookies }) => {
        const fetchData = Object.fromEntries(await request.formData());
        const body: any = {};
        
        if (fetchData.hasOwnProperty('birthdate')) {
            const realDate = new Date(fetchData.birthdate);
            realDate.setDate(realDate.getDate() + 1);
            fetchData.birthdate = realDate.getTime();            
        }

        Object.entries(fetchData).forEach(([key, value]) => {
            if (value != '')
                body[key] = value;
        });

        try {
            const req = await fetch(`${locals.api_path}/users/profile`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            });

            const { data, msg, errors } = await req.json();

            if (errors != null)
                throw locals.error(errors);

            return { data };
        } catch(error: any) {
            return fail(400, error);
        }
    }
}