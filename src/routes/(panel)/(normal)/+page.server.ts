import { redirect, fail } from '@sveltejs/kit';
import type { PageServerLoad, Actions } from './$types';

export const load: PageServerLoad = async ({ fetch, params, locals }) => {
    let patients, professional, addictions;

    try {
        const endpoint = (locals.session.role == 'professional') ? 'patients' : 'professional';
        const req = await fetch(`${locals.api_path}/users/me/${endpoint}`);
        const reqAddictions = await fetch(`${locals.api_path}/addictions`);
        const { data, msg, errors } = await req.json();
        const { data: dataAddictions } = await reqAddictions.json();

        if (errors != null)
            throw locals.error(errors);
        
        if (locals.session.role == 'professional') {
            patients = data.users ?? [];
            addictions = dataAddictions.addictions;
            patients = patients.map((user) => ({ ...user, addictionName: addictions.find((adi) => adi.id == user.addictionId).name }))
        }
        else if (locals.session.role == 'patient')
            professional = data.user ?? null;
    } catch (error: any) {
        return fail(400, error);
    }

    return { title: 'Inicio', patients, professional };
}

export const actions: Actions = {
    logout: async ({ cookies }) => {
        cookies.delete('_A', { path: '/' });
        cookies.delete('_I', { path: '/' });

        throw redirect(302, '/');
    }
}