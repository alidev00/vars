import { redirect, fail } from '@sveltejs/kit';
import type { PageServerLoad, Actions } from './$types';

export const load: PageServerLoad = async ({ fetch, params, locals }) => {
    return { title: 'Cuestionario del caso' };
}

export const actions: Actions = {
    saveQuiz: async ({ request, locals, fetch, cookies }) => {
        const fetchData = Object.fromEntries(await request.formData());
        const validation: any = {};
        const body = { context: '' };
        
        Object.entries(fetchData).forEach(([key, value]: any, index: number) => {
            body.context += `${index ? '%;%' : ''}${key}:=${value}`;
            validation[key as keyof typeof validation] = locals.EXP[key.replace(/\_\d+$/, '')].test(value);
        });

        if (Object.values(validation).some(val => !val))
            return fail(400, validation);
        
        try {
            const req = await fetch(`${locals.api_path}/users/profile`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            });
            const { data, msg, errors } = await req.json();

            if (errors != null)
                throw locals.error(errors);
        } catch(error: any) {
            return fail(400, error);
        }

        throw redirect(302, '/');
    }
}