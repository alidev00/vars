import type { ISessionInfo } from './session.ts';

export interface ILayoutData {
    session: ISessionInfo | undefined;
    title_base: string;
    title: string;
}