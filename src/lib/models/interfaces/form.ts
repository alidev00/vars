type Validation = boolean | null;

export interface IQuestionnaireData {
    [key: string]: string;
}

export interface IQuestionnaireValidation {
    [key: string]: Validation;
}

export interface ITreatmentPlanData {
    [key: string]: string;
}

export interface ITreatmentPlanValidation {
    [key: string]: Validation;
}

export interface ITreatmentPlanChangeEvent {
    (node: HTMLTextAreaElement): void
}

export interface IMedicalConsultingData {
    [key: string]: string;
}

export interface IMedicalConsultingValidation {
    [key: string]: Validation;
}

export interface IMedicalConsultingChangeEvent {
    (content: string): void
}

export interface ISupportData {
    subject: string;
    message: string;
}

export interface ISupportValidation {
    subject: Validation;
    message: Validation;
}
