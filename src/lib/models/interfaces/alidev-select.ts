import type { IPatientSignupAuthValidation, IProfessionalSignupAuthValidation, ILoginAuthValidation } from './session';

type ValidationChecks = {
    [key: string]: boolean | null;
}

export interface IOptionData {
    text: string;
    value: string | number;
}
export interface IAlidevData extends Array<IOptionData | undefined> {}
export interface IValidate {
    validation: IPatientSignupAuthValidation | IProfessionalSignupAuthValidation | ILoginAuthValidation;
    func: (node: Event & { currentTarget: EventTarget & HTMLSelectElement }) => void;
}
export interface IEventSelect {
    name: string;
    value: string | number;
    data: IAlidevData;
}