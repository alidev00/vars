type Notes = {
	description: string,
	emotion: number,
	date: string
}

export interface ISessionInfo {
	id: string;
    name: string;
    lastname: string;
    email: string;
    role: 'admin' | 'professional' | 'patient';
    notes?: Notes[];
    context?: string;
    isTreatment?: boolean;
    treatment?: '';
    birthdate?: number;
}

export interface ILoginAuthData {
	email: string;
	password: string;
}

export interface IPatientSignupAuthData extends ILoginAuthData  {
	name: string;
	lastname: string;
	birthdate: string;
	addictionId: string;
}

export interface IProfessionalSignupAuthData extends ILoginAuthData  {
	name: string;
	lastname: string;
	phone: string;
	title: string;
	nroDoctor: string;
}

export interface ILoginAuthValidation {
    email: null | boolean;
    password: null | boolean;
}

export interface IPatientSignupAuthValidation extends ILoginAuthValidation {
    name: null | boolean;
    lastname: null | boolean;
    birthdate: null | boolean;
    addictionId: null | boolean;
    email: null | boolean;
    password: null | boolean;
}

export interface IProfessionalSignupAuthValidation extends ILoginAuthValidation {
    name: null | boolean;
    lastname: null | boolean;
    phone: null | boolean;
    title: null | boolean;
    nroDoctor: null | boolean;
    email: null | boolean;
    password: null | boolean;
}

export interface ISignupAuthValidation {
    patient: IPatientSignupAuthValidation;
	professional: IProfessionalSignupAuthValidation;
}