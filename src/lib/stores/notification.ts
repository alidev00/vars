import { writable } from 'svelte/store';

const times = {};
export const notification: any = writable({});

export const addNotification = ({ type, msg }: any) => {
    const id = `${Date.now()}${crypto.randomUUID()}`.replace('-','_');

    notification.update((e: any) => ({ ...e, [id]: { type, msg } }));

    times[id] = setTimeout(() => {
        let _notification;
        
        notification.subscribe((e: any) => _notification = e);
        
        delete _notification[id];
        
        notification.update((e: any) => _notification);
    }, 3000);
}

export const removeNotification = (id: any) => {
    let _notification;

    clearTimeout(times[id]);    
    notification.subscribe((e: any) => _notification = e);
    delete _notification[id];
    notification.update((e: any) => _notification);
}