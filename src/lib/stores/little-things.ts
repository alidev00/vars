import { writable } from 'svelte/store';

export const showedPlanView = writable(false);
export const setShowedPlanView = (val) => showedPlanView.set(val);